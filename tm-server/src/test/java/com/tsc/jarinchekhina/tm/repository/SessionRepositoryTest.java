package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class SessionRepositoryTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Test
    public void testFindSession() {
        bootstrap.getUserService().create("auto", "auto");
        @Nullable final Session session = bootstrap.getSessionService().open("auto", "auto");
        Assert.assertNotNull(session);

        @NotNull List<Session> sessionList = bootstrap.getSessionService().getListSession(session);
        Assert.assertEquals(1, sessionList.size());

        bootstrap.getSessionService().remove(session);
        sessionList = bootstrap.getSessionService().findAll();
        Assert.assertEquals(0, sessionList.size());
        bootstrap.getUserService().removeByLogin("auto");
    }

}
