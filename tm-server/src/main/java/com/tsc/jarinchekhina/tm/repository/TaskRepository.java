package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.constant.FieldConst;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Task add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        return upsert(task);
    }

    @Override
    public void addAll(@NotNull final Collection<Task> collection) {
        for (@NotNull Task task : collection) {
            upsert(task);
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DELETE FROM `task`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM `task` WHERE `userId` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) return false;
        @NotNull final String query = "SELECT COUNT(*) FROM `task` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Long count() {
        @NotNull final String query = "SELECT COUNT(*) FROM `task`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getLong(1);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final String query = "SELECT * FROM `task`";
        @NotNull final PreparedStatement statement = prepareSql(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @SneakyThrows
    public Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConst.ID));
        task.setUserId(row.getString(FieldConst.USER_ID));
        task.setName(row.getString(FieldConst.NAME));
        task.setDescription(row.getString(FieldConst.DESCRIPTION));
        task.setDateStart(row.getDate(FieldConst.DATE_START));
        task.setDateFinish(row.getDate(FieldConst.DATE_FINISH));
        task.setStatus(prepareStatus(row.getString(FieldConst.STATUS)));
        task.setProjectId(row.getString(FieldConst.PROJECT_ID));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM `task` WHERE `userId` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "SELECT * FROM `task` WHERE `userId` = ? AND `projectId` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String id) {
        @NotNull final String query = "SELECT * FROM `task` WHERE `id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new ProjectNotFoundException();
        return fetch(resultSet);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "SELECT * FROM `task` WHERE `userId` = ? AND `id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new TaskNotFoundException();
        return fetch(resultSet);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM `task` WHERE `userId` = ? AND `name` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new TaskNotFoundException();
        return fetch(resultSet);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task insert(@NotNull final Task task) {
        @NotNull final String query = "INSERT INTO `task` " +
                "(`id`, `name`, `description`, `dateStart`, `dateFinish`, `userId`, `status`, `projectId`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setDate(4, prepare(task.getDateStart()));
        statement.setDate(5, prepare(task.getDateFinish()));
        statement.setString(6, task.getUserId());
        statement.setString(7, prepareStatus(task.getStatus()));
        statement.setString(8, task.getProjectId());
        statement.execute();
        return task;
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Task task) {
        @NotNull final String query = "DELETE FROM `task` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, task.getId());
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        @NotNull final String query = "DELETE FROM `task` WHERE `userId` = ? AND `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, task.getId());
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final String query = "DELETE FROM `task` WHERE `projectId` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, projectId);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        @NotNull final String query = "DELETE FROM `task` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "DELETE FROM `task` WHERE `userId` = ? AND `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, id);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Task task = findByIndex(userId, index);
        @NotNull final String query = "DELETE FROM `task` WHERE `userId` = ? AND `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, task.getId());
        statement.execute();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String query = "SELECT * FROM `task` WHERE `userId` = ? LIMIT ?, 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index - 1);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        boolean hasNext = resultSet.next();
        if (!hasNext) throw new TaskNotFoundException();
        return fetch(resultSet);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final String query = "DELETE FROM `task` WHERE `userId` = ? AND `name` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId, name);
        statement.execute();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String query = "UPDATE `task` " +
                "SET `userId` = ?, `name` = ?, `description` = ?, `dateStart` = ?, `dateFinish` = ?, `status` = ?, " +
                "`projectId` = ? " +
                "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(8, task.getId());
        statement.setString(1, task.getUserId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setDate(4, prepare(task.getDateStart()));
        statement.setDate(5, prepare(task.getDateFinish()));
        statement.setString(6, task.getStatus().toString());
        statement.setString(7, task.getProjectId());
        statement.execute();
        return task;
    }

}
