package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.AbstractEntity;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.List;

@Getter
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    private Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    public boolean isEmpty() {
        return count().equals(0L);
    }

    @NotNull
    public abstract Long count();

    @NotNull
    @Override
    public abstract List<E> findAll();

    @NotNull
    @Override
    public E add(@NotNull final E entity) {
        return upsert(entity);
    }

    @NotNull
    public E upsert(@NotNull final E entity) {
        if (contains(entity.getId())) return update(entity);
        return insert(entity);
    }

    @NotNull
    public abstract E insert(@NotNull final E entity);

    @NotNull
    public abstract E update(@NotNull final E entity);

    public abstract boolean contains(@NotNull final String id);

    @NotNull
    @Override
    public abstract E findById(@NotNull final String id);

    @Override
    public abstract void clear();

    @Override
    public abstract void removeById(@NotNull final String id);

    @SneakyThrows
    public PreparedStatement prepareSql(@NotNull final String query, @NotNull final String... args) {
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        for (int i = 0; i < args.length; i++) {
            statement.setString(i+1, args[i]);
        }
        return statement;
    }

    @Nullable
    public Date prepare(@Nullable final java.util.Date date) {
        if (date == null) return null;
        return new Date(date.getTime());
    }

    @Nullable
    public Role prepare(@Nullable final String value) {
        if (value == null) return null;
        return Role.valueOf(value);
    }

    @Nullable
    public String prepare(@Nullable final Role role) {
        if (role == null) return null;
        return role.toString();
    }

    @Nullable
    public Status prepareStatus(@Nullable final String value) {
        if (value == null) return null;
        return Status.valueOf(value);
    }

    @Nullable
    public String prepareStatus(@Nullable final Status status) {
        if (status == null) return null;
        return status.toString();
    }

}
