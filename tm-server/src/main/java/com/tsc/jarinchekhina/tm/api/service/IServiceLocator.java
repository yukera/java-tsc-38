package com.tsc.jarinchekhina.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    IConnectionService getConnectionService();

    @NotNull
    IDataService getDataService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

}
