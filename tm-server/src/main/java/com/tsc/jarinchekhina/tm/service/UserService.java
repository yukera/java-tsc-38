package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.*;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.HashIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.exception.user.EmailRegisteredException;
import com.tsc.jarinchekhina.tm.exception.user.LoginExistsException;
import com.tsc.jarinchekhina.tm.repository.UserRepository;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<User> collection) {
        if (collection == null) throw new AccessDeniedException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            userRepository.addAll(collection);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean checkRoles(@Nullable final String userId, @Nullable final Role... roles) {
        if (DataUtil.isEmpty(userId)) return false;
        if (roles == null) return true;
        @NotNull final User user = findById(userId);
        @NotNull final Role role = user.getRole();
        for (@NotNull final Role item : roles) {
            if (item.equals(role)) return true;
        }
        return false;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findById(id);
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            userRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (findByLogin(login) != null) throw new LoginExistsException(login);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
            if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
            user.setPasswordHash(hashPassword);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findByLogin(login);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        if (findByLogin(login) != null) throw new LoginExistsException(login);
        if (findByEmail(email) != null) throw new EmailRegisteredException(email);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setEmail(email);
            @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
            if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
            user.setPasswordHash(hashPassword);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findByEmail(email);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (findByLogin(login) != null) throw new LoginExistsException(login);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setRole(role);
            @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
            if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
            user.setPasswordHash(hashPassword);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final User user) {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            userRepository.remove(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            userRepository.removeById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            userRepository.removeByLogin(login);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @NotNull final User user = findById(userId);
            @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
            if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
            user.setPasswordHash(hashPassword);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final UserRepository userRepository = new UserRepository(connection);
            @NotNull final User user = findById(userId);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            connection.commit();userRepository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
