package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.Task;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    Task add(@NotNull String userId, @NotNull Task task);

    void clear(@NotNull String userId);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Task findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task findByName(@NotNull String userId, @NotNull String name);

    void remove(@NotNull String userId, @NotNull Task task);

    void removeAllByProjectId(@NotNull String projectId);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeByIndex(@NotNull String userId, @NotNull Integer index);

    void removeByName(@NotNull String userId, @NotNull String name);

}
