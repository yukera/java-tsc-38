package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ISessionRepository;
import com.tsc.jarinchekhina.tm.constant.FieldConst;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    public void addAll(@NotNull Collection<Session> collection) {
        for (@NotNull Session session : collection) {
            upsert(session);
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DELETE FROM `session`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) return false;
        @NotNull final String query = "SELECT COUNT(*) FROM `session` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Long count() {
        @NotNull final String query = "SELECT COUNT(*) FROM `session`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getLong(1);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NotNull final String query = "SELECT * FROM `session`";
        @NotNull final PreparedStatement statement = prepareSql(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @SneakyThrows
    public Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setId(row.getString(FieldConst.ID));
        session.setUserId(row.getString(FieldConst.USER_ID));
        session.setSignature(row.getString(FieldConst.SIGNATURE));
        session.setTimestamp(row.getLong(FieldConst.TIMESTAMP));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session findById(@NotNull final String id) {
        @NotNull final String query = "SELECT * FROM `session` WHERE `id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) throw new AccessDeniedException();
        return fetch(resultSet);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findByUserId(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM `session` WHERE `userId` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session insert(@NotNull final Session session) {
        @NotNull final String query = "INSERT INTO `session` " +
                "(`id`, `userId`, `signature`, `timestamp`) " +
                "VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getSignature());
        statement.setLong(4, session.getTimestamp() != null ? session.getTimestamp() : 0L);
        statement.execute();
        return session;
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Session session) {
        removeById(session.getId());
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        @NotNull final String query = "DELETE FROM `session` WHERE `id` = ?";
        @NotNull final PreparedStatement statement = prepareSql(query, id);
        statement.execute();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session update(@NotNull final Session session) {
        @NotNull final String query = "UPDATE `session` " +
                "SET `userId` = ?, `signature` = ?, `timestamp` = ? " +
                "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(4, session.getId());
        statement.setString(1, session.getUserId());
        statement.setString(2, session.getSignature());
        statement.setLong(3, session.getTimestamp() != null ? session.getTimestamp() : 0L);
        statement.execute();
        return session;
    }

}
