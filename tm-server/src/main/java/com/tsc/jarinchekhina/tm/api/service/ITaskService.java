package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task add(@Nullable String userId, @Nullable Task task);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

    void clear(@Nullable String userId);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    Task findById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task finishTaskById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task finishTaskByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task finishTaskByName(@Nullable String userId, @Nullable String name);

    void remove(@Nullable String userId, @Nullable Task task);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    void removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task startTaskById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task startTaskByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task startTaskByName(@Nullable String userId, @Nullable String name);

    Task updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
