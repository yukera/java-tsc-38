package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.endpoint.Task;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public void print(@NotNull final Task task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().value());
        System.out.println("PROJECT ID: " + task.getProjectId());
    }

}
