package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

public class TaskEndpointTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static Session session;

    @BeforeClass
    public static void before() {
        bootstrap.getUserEndpoint().createUser("autotest", "autotest");
        session = bootstrap.getSessionEndpoint().openSession("autotest", "autotest");
        Assert.assertNotNull(session);
    }

    @AfterClass
    public static void after() {
        bootstrap.getTaskEndpoint().clearTasks(session);
        @NotNull final Session adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
        bootstrap.getSessionEndpoint().closeSession(adminSession);
        bootstrap.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCreateFindRemove() {
        @Nullable Task taskById = bootstrap.getTaskEndpoint().createTask(session, "AUTO Task CFR");
        Assert.assertNotNull(taskById);
        @Nullable Task taskByIndex = bootstrap.getTaskEndpoint().createTask(session, "AUTO Task CFR 2");
        Assert.assertNotNull(taskByIndex);
        @Nullable Task taskByName = bootstrap.getTaskEndpoint().createTaskWithDescription(session, "AUTO Task CFR 3", "Desc");
        Assert.assertNotNull(taskByName);
        @NotNull List<Task> listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(3,listTasks.size());

        @NotNull String taskId = listTasks.get(0).getId();
        taskById = bootstrap.getTaskEndpoint().findTaskById(session, taskId);
        Assert.assertEquals(listTasks.get(0).getName(), taskById.getName());
        taskByIndex = bootstrap.getTaskEndpoint().findTaskByIndex(session, 2);
        Assert.assertEquals(listTasks.get(1).getName(), taskByIndex.getName());
        taskByName = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task CFR 3");
        Assert.assertEquals("AUTO Task CFR 3", taskByName.getName());

        taskId = bootstrap.getTaskEndpoint().findTaskByName(session, "AUTO Task CFR").getId();
        bootstrap.getTaskEndpoint().removeTaskById(session, taskId);
        bootstrap.getTaskEndpoint().removeTaskByName(session, "AUTO Task CFR 3");
        bootstrap.getTaskEndpoint().removeTaskByIndex(session, 1);
        listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(0, listTasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testUpdate() {
        @Nullable Task task = bootstrap.getTaskEndpoint().createTask(session, "AUTO Task Update");
        Assert.assertNotNull(task);

        @NotNull final String taskId = task.getId();
        task = bootstrap.getTaskEndpoint().updateTaskById(session, taskId, "AUTO Update","DESC");
        Assert.assertEquals("AUTO Update", task.getName());
        Assert.assertEquals("DESC", task.getDescription());

        task = bootstrap.getTaskEndpoint().updateTaskByIndex(session, 1, "JUNIT Update","JDESC");
        Assert.assertEquals("JUNIT Update", task.getName());
        Assert.assertEquals("JDESC", task.getDescription());

        bootstrap.getTaskEndpoint().removeTaskByName(session, "JUNIT Update");
        @NotNull final List<Task> listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(0, listTasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testChangeStatus() {
        @Nullable Task taskById = bootstrap.getTaskEndpoint().createTask(session, "AUTO Task Status");
        Assert.assertNotNull(taskById);
        @Nullable Task taskByIndex = bootstrap.getTaskEndpoint().createTask(session, "AUTO Task Status 2");
        Assert.assertNotNull(taskByIndex);
        @Nullable Task taskByName = bootstrap.getTaskEndpoint().createTaskWithDescription(session, "AUTO Task Status 3", "Desc");
        Assert.assertNotNull(taskByName);

        @NotNull final String taskId = taskById.getId();
        Assert.assertEquals(Status.NOT_STARTED, taskById.getStatus());
        taskById = bootstrap.getTaskEndpoint().startTaskById(session, taskId);
        Assert.assertEquals(Status.IN_PROGRESS, taskById.getStatus());
        taskById = bootstrap.getTaskEndpoint().finishTaskById(session, taskId);
        Assert.assertEquals(Status.COMPLETED, taskById.getStatus());
        taskById = bootstrap.getTaskEndpoint().changeTaskStatusById(session, taskId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskById.getStatus());

        Assert.assertEquals(Status.NOT_STARTED, taskByIndex.getStatus());
        taskByIndex = bootstrap.getTaskEndpoint().startTaskByIndex(session, 1);
        Assert.assertEquals(Status.IN_PROGRESS, taskByIndex.getStatus());
        taskByIndex = bootstrap.getTaskEndpoint().finishTaskByIndex(session, 1);
        Assert.assertEquals(Status.COMPLETED, taskByIndex.getStatus());
        taskByIndex = bootstrap.getTaskEndpoint().changeTaskStatusByIndex(session, 1, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskByIndex.getStatus());

        Assert.assertEquals(Status.NOT_STARTED, taskByName.getStatus());
        taskByName = bootstrap.getTaskEndpoint().startTaskByName(session, "AUTO Task Status 3");
        Assert.assertEquals(Status.IN_PROGRESS, taskByName.getStatus());
        taskByName = bootstrap.getTaskEndpoint().finishTaskByName(session, "AUTO Task Status 3");
        Assert.assertEquals(Status.COMPLETED, taskByName.getStatus());
        taskByName = bootstrap.getTaskEndpoint().changeTaskStatusByName(session, "AUTO Task Status 3", Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, taskByName.getStatus());

        bootstrap.getTaskEndpoint().removeTaskByName(session, "AUTO Task Status");
        bootstrap.getTaskEndpoint().removeTaskByName(session, "AUTO Task Status 2");
        bootstrap.getTaskEndpoint().removeTaskByName(session, "AUTO Task Status 3");
        @NotNull final List<Task> listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(0,listTasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testBindUnbindTask() {
        @Nullable Project project = bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @Nullable Task taskFirst = bootstrap.getTaskEndpoint().createTask(session, "AUTO Task Binding");
        @Nullable Task taskSecond = bootstrap.getTaskEndpoint().createTask(session, "AUTO Task Binding 2");

        taskFirst = bootstrap.getTaskEndpoint().bindTaskByProjectId(session, projectId, taskFirst.getId());
        Assert.assertNotNull(taskFirst);
        taskSecond = bootstrap.getTaskEndpoint().bindTaskByProjectId(session, projectId, taskSecond.getId());
        Assert.assertNotNull(taskSecond);
        @NotNull List<Task> tasks = bootstrap.getTaskEndpoint().findAllTaskByProjectId(session, projectId);
        Assert.assertEquals(2, tasks.size());

        taskSecond = bootstrap.getTaskEndpoint().unbindTaskByProjectId(session, taskSecond.getId());
        Assert.assertNotNull(taskSecond);
        tasks = bootstrap.getTaskEndpoint().findAllTaskByProjectId(session, projectId);
        Assert.assertEquals(1, tasks.size());

        bootstrap.getTaskEndpoint().removeTaskById(session, taskFirst.getId());
        bootstrap.getTaskEndpoint().removeTaskById(session, taskSecond.getId());
        bootstrap.getProjectEndpoint().removeProjectById(session, projectId);
        @NotNull final List<Task> listTasks = bootstrap.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(0,listTasks.size());
    }

}
